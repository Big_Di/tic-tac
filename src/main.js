function main() {
  const imgZero = `
    <svg class="icon icon-radio-unchecked" viewBox="0 0 32 32">
      <path d="M16 0c-8.837 0-16 7.163-16 16s7.163 16 16 16 16-7.163 16-16-7.163-16-16-16zM16 28c-6.627 0-12-5.373-12-12s5.373-12 12-12c6.627 0 12 5.373 12 12s-5.373 12-12 12z"></path>
    </svg>
  `;

  const imgCross  = `
    <svg class="icon icon-cross" viewBox="0 0 32 32">
      <path d="M31.708 25.708c-0-0-0-0-0-0l-9.708-9.708 9.708-9.708c0-0 0-0 0-0 0.105-0.105 0.18-0.227 0.229-0.357 0.133-0.356 0.057-0.771-0.229-1.057l-4.586-4.586c-0.286-0.286-0.702-0.361-1.057-0.229-0.13 0.048-0.252 0.124-0.357 0.228 0 0-0 0-0 0l-9.708 9.708-9.708-9.708c-0-0-0-0-0-0-0.105-0.104-0.227-0.18-0.357-0.228-0.356-0.133-0.771-0.057-1.057 0.229l-4.586 4.586c-0.286 0.286-0.361 0.702-0.229 1.057 0.049 0.13 0.124 0.252 0.229 0.357 0 0 0 0 0 0l9.708 9.708-9.708 9.708c-0 0-0 0-0 0-0.104 0.105-0.18 0.227-0.229 0.357-0.133 0.355-0.057 0.771 0.229 1.057l4.586 4.586c0.286 0.286 0.702 0.361 1.057 0.229 0.13-0.049 0.252-0.124 0.357-0.229 0-0 0-0 0-0l9.708-9.708 9.708 9.708c0 0 0 0 0 0 0.105 0.105 0.227 0.18 0.357 0.229 0.356 0.133 0.771 0.057 1.057-0.229l4.586-4.586c0.286-0.286 0.362-0.702 0.229-1.057-0.049-0.13-0.124-0.252-0.229-0.357z"></path>
    </svg>
  `;

  const TOTAL_FOR_WIN = 3;
  const CELL_VALUE = Object.freeze({
    CROSS: 'X',
    ZERO: 'O',
    EMPTY: '-'
  });
  const SIDE = Object.freeze({
    PLAYER: 'player',
    COMPUTER: 'computer',
  });

  function getNewMatrix(n, val) {
    const matrix = [];
    for(let i = 0; i < n; i++){
      const arr = [];
      for(let j = 0; j < n; j++) {
        arr.push(val);
      }
      matrix.push(arr);
    }
    return matrix
  }

  function eachMatrix(matrix, callback) {
    for(let i = 0; i < matrix.length; i++) {
      const row = matrix[i];
      for(let j = 0; j < row.length; j++){
        callback(matrix[i][j], i, j);
      }
    }
  }

  function YesNo(){ return Math.random() >= 0.5; }

  function isNotEmpty(cell) {
    return cell === CELL_VALUE.CROSS || cell === CELL_VALUE.ZERO;
  }
  function checkVictory(data) {
    const matrix = data.matrix;
    // ROWs
    if(isNotEmpty(matrix[0][0]) && matrix[0][0] === matrix[0][1] && matrix[0][1] === matrix[0][2]) {
      lightCell(0, 0, data);
      lightCell(0, 1, data);
      lightCell(0, 2, data);
      return {isFinish: true, win: matrix[0][0]};
    }

    if(isNotEmpty(matrix[1][0]) && matrix[1][0] === matrix[1][1] && matrix[1][1] === matrix[1][2]) {
      lightCell(1, 0, data);
      lightCell(1, 1, data);
      lightCell(1, 2, data);
      return {isFinish: true, win: matrix[1][0]};
    }

    if(isNotEmpty(matrix[2][0]) && matrix[2][0] === matrix[2][1] && matrix[2][1] === matrix[2][2]) {
      lightCell(2, 0, data);
      lightCell(2, 1, data);
      lightCell(2, 2, data);
      return {isFinish: true, win: matrix[2][0]};
    }

    // COLs
    // C0
    if(isNotEmpty(matrix[0][0]) && matrix[0][0] === matrix[1][0] && matrix[1][0] === matrix[2][0]) {
      lightCell(0, 0, data);
      lightCell(1, 0, data);
      lightCell(2, 0, data);
      return {isFinish: true, win: matrix[0][0]};
    }

    // C1
    if(isNotEmpty(matrix[0][1]) && matrix[0][1] === matrix[1][1] && matrix[1][1] === matrix[2][1]) {
      lightCell(0, 1, data);
      lightCell(1, 1, data);
      lightCell(2, 1, data);
      return {isFinish: true, win: matrix[0][1]};
    }

    // C2
    if(isNotEmpty(matrix[0][2]) && matrix[0][2] === matrix[1][2] && matrix[1][2] === matrix[2][2]) {
      lightCell(0, 2, data);
      lightCell(1, 2, data);
      lightCell(2, 2, data);
      return {isFinish: true, win: matrix[0][2]};
    }
    //  00 -> 22
    if(isNotEmpty(matrix[0][0]) && matrix[0][0] === matrix[1][1] && matrix[1][1] === matrix[2][2]) {
      lightCell(0, 0, data);
      lightCell(1, 1, data);
      lightCell(2, 2, data);
      return {isFinish: true, win: matrix[0][0]};
    }

    // 02 -> 20
    if(isNotEmpty(matrix[0][2]) && matrix[0][2] === matrix[1][1] && matrix[1][1] === matrix[2][0]) {
      lightCell(0, 2, data);
      lightCell(1, 1, data);
      lightCell(2, 0, data);
      return {isFinish: true, win: matrix[0][2]};
    }

    let isFull = true;
    eachMatrix(matrix, (item) => {
      if(isFull === false) { return;}
      if(item === CELL_VALUE.EMPTY) {
        isFull = false;
      }
    });

    if(isFull) {
      return {
        isFinish: true,
        win: CELL_VALUE.EMPTY
      }
    }

    return {
      isFinish: false
    }
  }

  function lightCell(row, col, data) {
    data.lights.push({
      row, col
    })
  }

  function setCurrentValueCell (data, item) {
    if(item === CELL_VALUE.EMPTY) {
      if(data.move === SIDE.PLAYER) {
        // data.move = SIDE.COMPUTER;
        return data.player;
      } else if(data.move === SIDE.COMPUTER) {
        // data.move = SIDE.PLAYER;
        return data.computer;
      }
    }
    return item;
  }


  /**
   * Returns a random integer between min (inclusive) and max (inclusive)
   * Using Math.round() will give you a non-uniform distribution!
   */
  function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  function createData () {
    return {
      nextGame: function () {
        this.lock = false;
        this.lights = [];
        this.matrix = getNewMatrix(3, CELL_VALUE.EMPTY);
        this.player = this.player === CELL_VALUE.CROSS ?  CELL_VALUE.ZERO :  CELL_VALUE.CROSS;
        this.computer = this.computer === CELL_VALUE.CROSS ?  CELL_VALUE.ZERO :  CELL_VALUE.CROSS;
        if(this.computer === CELL_VALUE.CROSS) {
          nextStepByComputer(this);
        }
        render(this);
      },
        resetGame: function () {
          this.lock = false;
          this.lights= [];
          this.matrix= getNewMatrix(3, CELL_VALUE.EMPTY);
          this.computer_wins_counter= 0;
          this.player_wins_counter= 0;
          this.total_games= 0;
          this.move= SIDE.PLAYER;
          this.player= CELL_VALUE.CROSS;
          this.computer=CELL_VALUE.ZERO;
        render(this);
      },
      lock: false,
      lights: [],
      matrix: getNewMatrix(3, CELL_VALUE.EMPTY),
      computer_wins_counter: 0,
      player_wins_counter: 0,
      total_games: 0,
      move: SIDE.PLAYER,
      player: CELL_VALUE.CROSS,
      computer: CELL_VALUE.ZERO
    };
  }

  function nextStepByComputer(data) {
    let freeCells = [];

    eachMatrix(data.matrix,  (item, row, col)=> {
      if(item === CELL_VALUE.EMPTY) {
        freeCells.push({row, col});
      }
    });

    if(freeCells.length === 1) {
      data.matrix[freeCells[0].row][freeCells[0].col] = data.computer;
    } else if(freeCells.length > 1) {
      let celIdx = getRandomInt(0, freeCells.length -1);
      data.matrix[freeCells[celIdx].row][freeCells[celIdx].col] = data.computer;
    }

    const result = checkVictory(data);
    if(result.isFinish) {
      data.lock = true;

      document
        .querySelector(".start__btn")
        .removeAttribute("disabled");

      data.total_games++;
      if(result.win === data.player) {
        data.player_wins_counter++;
      } else if(result.win === data.computer) {
        data.computer_wins_counter++;
      }
      if(data.player_wins_counter === TOTAL_FOR_WIN) {
        render(data);
        alert('Win player');
        data.resetGame();
        render(data);
        document
          .querySelector(".start__btn")
          .setAttribute("disabled", "disabled");
      } else if(data.computer_wins_counter === TOTAL_FOR_WIN) {
        render(data);
        alert('Win computer');
        data.resetGame();
        render(data);
        document
          .querySelector(".start__btn")
          .setAttribute("disabled", "disabled");
      }
    }

    render(data);

  }

  function render(data) {
    console.log('render\n\n');
    console.log(data.matrix[0]);
    console.log(data.matrix[1]);
    console.log(data.matrix[2]);
    console.log('\n\n');

    document
      .querySelector('#computer_wins_counter')
      .innerText = `${data.computer_wins_counter}/${TOTAL_FOR_WIN}`;

    document
      .querySelector('#player_wins_counter')
      .innerText = `${data.player_wins_counter}/${TOTAL_FOR_WIN}`;

    document
      .querySelector('#total_games')
      .innerText = `${data.total_games}`;

    let list = document.querySelector('#list');
    list.innerHTML = '';// clear
    eachMatrix(data.matrix, (item, i, j) => {

      const handleOnClick = () => {
        if( data.lock || isNotEmpty(data.matrix[i][j])) {return;}
        data.matrix[i][j] = setCurrentValueCell(data, item);

        const result = checkVictory(data);
        if(result.isFinish) {
          data.lock = true;

          document
            .querySelector(".start__btn")
            .removeAttribute("disabled");

          data.total_games++;
          if(result.win === data.player) {
            data.player_wins_counter++;
          } else if(result.win === data.computer) {
            data.computer_wins_counter++;
          }
          if(data.player_wins_counter === TOTAL_FOR_WIN) {
            render(data);
            alert('Win player');
            data.resetGame();
            render(data);
            document
              .querySelector(".start__btn")
              .setAttribute("disabled", "disabled");
          } else if(data.computer_wins_counter === TOTAL_FOR_WIN) {
            render(data);
            alert('Win computer');
            data.resetGame();
            render(data);
            document
              .querySelector(".start__btn")
              .setAttribute("disabled", "disabled");
          }

        } else {
          nextStepByComputer(data);
        }
        console.log('result victory', result);
        render(data);
      };

      const btn = document.createElement('button');
      btn.classList.add('btn__img');

      if(data.lights.find(cell => cell.row === i && cell.col === j)){
        btn.classList.add('btn__light');
      }

      btn.dataset.row = i;
      btn.dataset.col = j;

      if(item === CELL_VALUE.EMPTY) {
        btn.innerHTML = '';
      } else if(item === CELL_VALUE.CROSS) {
        btn.innerHTML = imgCross;
      } else if(item === CELL_VALUE.ZERO) {
        btn.innerHTML = imgZero;
      }

      btn.addEventListener('click', handleOnClick);
      list.appendChild(btn);
    });
  }

  const data = createData();
  render(data);

  document
    .querySelector(".start__btn").addEventListener('click', (event)=> {
      event.target.setAttribute("disabled", "disabled");
      data.nextGame();
  });
}
